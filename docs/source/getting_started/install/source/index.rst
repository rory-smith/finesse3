.. include:: ../../../defs.hrst
.. _source-installation:

Installing from source
======================

Installing |Finesse| from source means you can get access to mulitple versions,
including the latest development features. This method should only be used by people who
want to run latest development release or want to make changes to the way |Finesse|
operates. Regular users should use :ref:`installation-pypi` or :ref:`installation-conda`
instead.

If you are planning on making changes to |Finesse| please read the developer
documentation.

The |Finesse| source code is managed using the versioning control software `git
<https://git-scm.com/>`__.

It's difficult to provide build instructions for all possible systems and
configurations. Below we detail the most common ways we know of or use ourselves as
developers for building |Finesse|. If you run into issues please contact us.

.. warning::

    The following descriptions may not include any required activation step for the
    particular Python environment that |Finesse| will be installed into. We assume if
    you are installing from source you will be activating a Python environment of your
    own choosing and you know how to do this. If you do not, we recommend using `pip and
    virtualenv
    <https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/>`__
    or `Conda environments
    <https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html>`__.

.. toctree::

    windows
    mac
    linux
