.. include:: /defs.hrst

==============
Gaussian beams
==============

.. kat:element:: cavity

.. kat:element:: gauss

    .. jupyter-execute::
        :hide-code:

        from finesse.utilities.misc import doc_element_parameter_table
        from finesse.components import Gauss
        doc_element_parameter_table(Gauss)
